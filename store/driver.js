export const state = () => {
  return {
    profile: null
  }
}

export const mutations = {
  updateProfile(state, data) {
    state.profile = data;
  }
}
