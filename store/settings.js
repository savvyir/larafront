export const state = () => ({
  data: null,
  sellTypes: [
    {name: 'checque', title: 'چک'},
    {name: 'chash', title: 'نقدی'},
    {name: 'other', title: 'توافقی'},
  ],
  creditTypes: [
    {name: 'cheque', title: 'چک'},
    {name: 'promissory', title: 'سفته'},
    {name: 'invoice', title: 'فاکتور/صرسید'},
    {name: 'material', title: 'محصول'},
    {name: 'service', title: 'خدمت'},
    {name: 'other', title: 'توافقی'},
  ],
  paymentTypes: [
    {name: 'cash', title: 'نقدی/واریز به حساب'},
    {name: 'cheque', title: 'چک'},
    {name: 'promissory', title: 'سفته'},
    {name: 'material', title: 'محصول'},
    {name: 'service', title: 'خدمت'},
    {name: 'other', title: 'دیگر'},
  ],
  debtTypes: [
    {name: 'cheque', title: 'نقدی/واریز به حساب'},
    {name: 'cheque', title: 'چک'},
    {name: 'promissory', title: 'سفته'},
    {name: 'invoice', title: 'فاکتور/رسید'},
    {name: 'material', title: 'محصول'},
    {name: 'service', title: 'خدمت'},
    {name: 'other', title: 'توافقی'},
  ]
});

export const mutations = {
  setData(state, data) {
    state.data = data
  },
};
