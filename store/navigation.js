
export const state = () => ({
  meta: null,
  title: null,
  description: null,
  path: null,
  drawer: false,
  dialog: false,
  isMobile: false
})

export const mutations = {
  setDrawer(state, val) {
    state.drawer = val
  }, setDialog(state, val) {
    state.dialog = val
  }, setMeta(state, items) {
    state.meta = items
  },
  pushMeta(state, items) {
    state.meta.push(items)
  },
  setTitle(state, title) {
    state.title = title
  },
  setDescription(state, val) {
    state.description = val
  },
  setPath(state, data) {
    state.path = data
  }
}
