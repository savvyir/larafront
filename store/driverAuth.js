export const state = () => ({
    phone: "",
    token: null,
})

export const mutations = {
    updatePhone(state, data) {
        state.phone = data
    },
    updateToken(state, data) {
        state.token = data
    },
}

export const actions = {
    logout: function ({commit}) {
        localStorage.removeItem("driverToken");
        commit("updateToken", null)
    }
}
