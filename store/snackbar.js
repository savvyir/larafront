export const state = () => ({
  snack: "",
  color: "info"
})

export const mutations = {
  setSnack(state, snack, color) {
    state.snack = snack
  },
  msg(state, snack, color) {
    state.snack = snack
  },
  setColor(state, color = "info") {
    state.color = color
  }
}
