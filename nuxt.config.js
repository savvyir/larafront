import colors from 'vuetify/es5/util/colors'
import fa from 'vuetify/es5/locale/fa'
import constants from "./assets/constants";

module.exports = {
    mode: 'spa',
    /*
    ** Headers of the page
    */
    head: {
        titleTemplate: '%s',
        title: 'دیجی دلیوری',
        meta: [
            {charset: 'utf-8'},
            {name: 'viewport', content: 'width=device-width, initial-scale=1'},
            {
                hid: 'description',
                name: 'description',
                content: 'سیستم ارسال بار درون شهری',
            },
            {hid: 'author', name: 'author', content: 'دیجی دلیوری - پلتفرم مدیریت سنگبری'},
            {hid: 'og:description', name: 'og:description', content: 'برای مشاهده ی امکانات روی لینک کلیک کنید.'},
            {hid: 'og:title', name: 'og:title', content: 'دیجی دلیوری - ارسال بار درون شهری'},
            {hid: 'og:site_name', name: 'og:site_name', content: 'دیجی دلیوری - ارسال بار درون شهری'},
            {
                hid: 'apple-mobile-web-app-title',
                name: 'apple-mobile-web-app-title',
                content: 'دیجی دلیوری'
            },
            {
                hid: 'keywords',
                name: 'keywords',
                content: 'دیجی دلیوری,digidelivery,دیجی دلیوری آی آر'
            }
        ],
        script: [
            //{src: 'https://api.cedarmaps.com/cedarmaps-gl.js/v2.2.0/cedarmaps-gl.js'},
            ///{src: 'https://api.cedarmaps.com/cedarmaps.js/v1.8.1/cedarmaps.js'},
            //{src: '/cedar/src/cedarmaps.js'},
            {src: '/cedar/dist/v1.8.1/cedarmaps.js'},
            {src: '/cedar/demos/js/leaflet-src.js'},
            {src: '/cedar/demos/js/leaflet-omnivore.min.js'},
        ],
        link: [
            //{rel: "stylesheet", href: 'https://api.cedarmaps.com/cedarmaps-gl.js/v2.2.0/cedarmaps-gl.css'},
            //{rel: "stylesheet", href: '/cedar/dist/v1.8.1/cedarmaps.css'},
            {rel: "stylesheet", href: 'https://demo.cedarmaps.com/websdk/dist/v1.8.1/cedarmaps.css'},
        ]

    },
    /*
    ** Customize the progress-bar color
    */
    loading: {color: '#fff'},
    /*
    ** Global CSS
    */

    css: [
        "~assets/variables",
        "~assets/font",
        "~assets/style/custom",
        "~assets/style/helper.styl",
    ],

    /*
    ** Plugins to load before mounting the App
    */
    plugins: [
        "@/plugins/axios.js",
        "@/plugins/utility.js",
        {src: "@/plugins/cedarmap.js", mode: 'client'}
    ],
    /*
    ** Nuxt.js dev-modules
    */
    buildModules: [
        '@nuxtjs/vuetify',
        '@nuxtjs/axios',
        '@nuxtjs/pwa',
    ],
    /*
    ** Nuxt.js modules
    */
    modules: [
        // Doc: https://axios.nuxtjs.org/usage
        '@nuxtjs/axios',
        '@nuxtjs/pwa',
        // Doc: https://github.com/nuxt-community/dotenv-module
        '@nuxtjs/dotenv',
    ],
    /*
    ** Axios module configuration
    ** See https://axios.nuxtjs.org/options
    */
    axios: {
        baseURL: constants.API_URL
    },

    /*
    ** Build configuration
    */
    build: {
        /*
        ** You can extend webpack config here
        */
        extend(config, ctx) {
        }
    },
    server: {
        port: 3000, // default: 3000
        host: '0.0.0.0', // default: localhost
    },
    /*
    ** vuetify module configuration
    ** https://github.com/nuxt-community/vuetify-module
    */

    vuetify: {
        rtl: true,
        lang: {
            locales: {fa},
            current: 'fa',
        },
        icons: {
            iconfont: 'mdiSvg || mdi' || 'md' || 'fa' || 'fa4'
        },
        customVariables: ['~/assets/variables.scss'],
        theme: {
            //dark: true,
            themes: {
                light: {
                    primary: colors.teal.base,
                    accent: colors.cyan.lighten4,
                    secondary: colors.amber.base,
                    info: colors.blue.lighten1,
                    warning: colors.amber.base,
                    error: colors.deepOrange.base,
                    success: colors.green.base
                },
            }
        }
    },
}
