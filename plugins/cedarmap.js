import Vue from 'vue'
import MapHelper from '~/assets/MapHelper.js'

Vue.set(Vue.prototype, '$MapHelper', MapHelper);
