/*
const moment = require('moment-jalaali');
const apiServer = 'https://api.koopi.ir'

const apiUrl = 'https://erpback.koopi.ir'

import Strapi from 'strapi-sdk-javascript/build/main'

const strapi = new Strapi(apiUrl)

import Helper from '~/assets/js/helper.js'
import Constants from '~/assets/js/constants.js'
import VuePersianDatetimePicker from 'vue-persian-datetime-picker';

import VueHtmlToPaper from 'vue-html-to-paper';
*/
import Vue from 'vue'
//import Mapbox from '~/assets/mapbox.js'
import Helper from '~/assets/Helper.js'
import Constants from '~/assets/constants.js'

const moment = require('moment-jalaali');
const _ = require('lodash');
const apiServer = 'http://api.local';

Vue.set(Vue.prototype, '_', _);
Vue.set(Vue.prototype, 'Moment', moment);
Vue.set(Vue.prototype, 'ApiServer', apiServer);
Vue.set(Vue.prototype, 'Helper', Helper);
Vue.set(Vue.prototype, 'Constants', Constants);
//Vue.set(Vue.prototype, 'Mapbox', Mapbox);
/*
const options = {
  name: '_blank',
  specs: [
    'fullscreen=yes',
    'titlebar=yes',
    'scrollbars=yes'
  ],
  styles: [
    'https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css',
    'https://unpkg.com/kidlat-css/css/kidlat.css'
  ]
}

Vue.use(VueHtmlToPaper, options);

// or, using the defaults with no stylesheet

Vue.use(VueHtmlToPaper);

Vue.use(VuePersianDatetimePicker, {
  name: 'date-picker',
  props: {
    editable: true,
    autoSubmit: true,
  }
});

Vue.set(Vue.prototype, '$strapi', strapi);
Vue.set(Vue.prototype, 'Helper', Helper);
Vue.set(Vue.prototype, 'Moment', moment);
Vue.set(Vue.prototype, 'ApiServer', apiServer);
Vue.set(Vue.prototype, 'Helper', Helper);
Vue.set(Vue.prototype, 'Constants', Constants);

export default strapi;
export {apiUrl}
*/
