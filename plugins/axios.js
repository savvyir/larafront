export default function ({$axios, redirect, store}) {
    if (localStorage.getItem("driverToken")) {
        $axios.setToken(localStorage.getItem("driverToken"), 'Bearer');
        store.commit('driverAuth/updateToken', localStorage.getItem("driverToken"))
    }
    $axios.onRequest(config => {
        // console.log('Making request to ' + config.url)
    })

    $axios.onError(error => {
        const code = parseInt(error.response && error.response.status)
        if (code === 401 || code === 403) {
            store.dispatch('driverAuth/logout');
            redirect('/auth');
        } else if (code > 400) {
            let statusPath = 'data.success',
                msgPath = 'data.msg',
                status = _.get(error.response, statusPath, null),
                msg = _.get(error.response, msgPath, null);
            if (status) {
                store.commit("snackbar/setColor", 'info')
            } else {
                store.commit("snackbar/setColor", 'warning')
            }
            store.commit("snackbar/setSnack", msg)
        }
    })
}
