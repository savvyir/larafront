export default {
    DEFAULT_PHOTO: 'http://koopi.ir/dummy/avatar.png',
    API_URL: 'http://api.local/api',
    PER_PAGE: 6,
    USER_DRAWER: [
        {title: 'دریافت سفارش', icon: 'dashboard', link: '/driver/map'},
        {title: 'سفارش های قبلی', icon: 'shop', link: '/driver/orders'},
        //{title: 'تیکت/پشتیبانی', icon: 'help', link: '/driver/tickets'},
        {title: 'لیست تراکنش ها', icon: 'dashboard', link: '/driver/tokens'},
        {title: 'ویرایش پروفایل', icon: 'account_circle', link: '/driver/profile/edit'},
    ],
    FOOTER_LINKS: [
        {title: 'درباره ی کوپی', icon: 'contact', link: '/page/about-us'},
        {title: 'قوانین کوپی', icon: 'contact', link: '/page/rules'},
        {title: 'راهنما', icon: 'contact', link: '/page/help'},
        {title: 'سوالات متداول', icon: 'contact', link: '/page/faq'},
        {title: 'پیشنهادات و شکایات', icon: 'contact', link: '/page/contact-us'},
    ],

}
