const coordinates = [35.7, 51.355];

export default {

    initial(id = 'map', token = '8fd4e460797c0dd2ce2cb11eeb8143ff5bd3de9c', zoom = 14) {
        try {
            L.cedarmaps.accessToken = token;
        } catch (err) {
        }

        /**
         * Initilizing Map View
         */

            // Getting maps info from a tileJSON source
        var tileJSONUrl = 'https://api.cedarmaps.com/v1/tiles/cedarmaps.streets.json?access_token=' + L.cedarmaps.accessToken;

        // initilizing map into div#map
        var map = L.cedarmaps.map(id, tileJSONUrl, {
            scrollWheelZoom: true
        }).setView(coordinates, 12);
        return map;
    },
    marker(map, coordinate = coordinates, icon = null, popup = null) {
        let marker = L.marker(coordinate);
        if (icon) {
            marker = L.marker(coordinate, {icon})
        }
        marker = marker.addTo(map);
        if (popup) marker.bindPopup(popup).openPopup();
        return marker;
    },
    popup(map, html, coordinate = coordinates) {

    },
    direction(route = '35.764335,51.365622;35.7604311,51.3939486;35.7474946,51.2429727', map) {

    }
}
