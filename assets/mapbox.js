export default {
  mapbox(map) {
    const mapboxgl = require('mapbox-gl/dist/mapbox-gl.js');
    var coordinates = [51.355, 35.7];
    var zoom = 10;

    mapboxgl.accessToken =
      "pk.eyJ1Ijoic2F2dnl2ZXJzYSIsImEiOiJjazQ4bDJkdnkwOWRmM2RsaDBqNHNhdXVwIn0.4Jx3H5ySFBwe2-jRaf4w1g";
    var map = new mapboxgl.Map({
      container: "map",
      zoom,
      center: coordinates,
      style: "mapbox://styles/mapbox/streets-v9"
    });

    mapboxgl.setRTLTextPlugin('https://api.mapbox.com/mapbox-gl-js/plugins/mapbox-gl-rtl-text/v0.2.0/mapbox-gl-rtl-text.js');
    const pluginStatus = mapboxgl.getRTLTextPluginStatus();
    var MapboxLanguage = require('@mapbox/mapbox-gl-language');

    var size = 200;


    var mabdaMarker = new mapboxgl.Marker({
      draggable: true,
      anchor:'center',
    }).setLngLat(coordinates).addTo(map);

    var hasMabda = false;
    var hasMaghsad = false;

    var mabdaCoordinate = [];
    var maghsadCoordinate = [];
    var maghsadMarker;
    map.on('click', function (e) {
      let lng = e.lngLat.lng;
      let lat = e.lngLat.lat;
      if (!hasMabda) {
        mabdaMarker.setLngLat([lng, lat])
        maghsadMarker = new mapboxgl.Marker({
          draggable: true
        }).setLngLat([lng, lat]).addTo(map);
        hasMabda = true;
        mabdaMarker.setDraggable(false)
      } else if (hasMabda && !hasMaghsad) {
        maghsadCoordinate = maghsadMarker;
        maghsadMarker.setLngLat([lng, lat]);
        hasMaghsad = true;
        maghsadMarker.setDraggable(false)
      } else if (hasMaghsad) {
        alert('لطفا نحوه ی ارسال را انتخاب کنید')
      }

    })


  }
}
