export default function ({store, redirect}) {
  if (!localStorage.getItem("driverToken")) {
    return redirect('/auth')
  }
}
