export default function ({store, route}) {
  if (_.has(route, 'meta')) {
    let metadata = _.get(route, 'meta', []);
    store.commit("navigation/setMeta", metadata)
    store.commit("navigation/setTitle", _.get(metadata, `[${metadata.length - 1}].title`, ""))
    store.commit("navigation/setDescription", _.get(metadata, `[${metadata.length - 1}].description`, ""))
    store.commit("navigation/setPath", route.path)
  }
}
